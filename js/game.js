// Fonction jQuery : exécute le jeu une fois que le DOM est chargé

$(function() {


var remake = $('.container-fluid').children();
$('.container-fluid').remove();

menu(remake);

//lunchgame(remake);
})
function lunchgame(remake, pseudo, difficultie){
	$('.container-fluid').remove();
	$('.restartgame').remove();
	$('body').append('<div class="container-fluid"></div>');
	$('.container-fluid').html(remake);
	

	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */

	// Objet littéral qui stocke l'argent, les vies et la vitesse du jeu
	var	Player = {
			money: 600,
			life : 20,
			speed: 4, // 10 = fast; 50 = normal mode
			time : 5, // time (in sec) before monsters move
			retime: 5,
			level: 1,
			pseudo: pseudo,
			difficultie: difficultie,//1 = facile, 3 = difficile
			
		}; 
	
		var nbmonstersmax = 5;
		var maxlevel = 21;

	/* ---------- ---------- */
	/* ------ PARCOURS ----- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le parcours des monstres
	var	Parcours = {
			start: 600, 
			sizeCourse: 110,
			course: [
				['down' ,200],
				['left' ,500],
				['down' ,250],
				['right',800],
				['up',400],
				['right',400],
				['down',600],
			]
		};


	// On appelle la fonction qui crée le parcours (visuel)
	makeCourse(Parcours);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	// On affiche les tours que l'on peut créer à l'écran
	displayTowers(Player, towers); 

	// On appelle la fonction qui permet de créer des tours
	makeTowers(towers, Player);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu

	// On appelle la fonction qui permet de créer des monstres
	makeMonsters(monsters, Parcours, nbmonstersmax);

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */

	// On appelle la fonction qui lance le jeu
	startGame(Player, Parcours, monsters, towers, maxlevel);

	reset(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel);

	restart(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel, remake);
}

// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

// Fonction qui déclare les monstres à créer et les stocke dans le tableau des monstres
function makeMonsters(monsters, Parcours, nbmonstersmax) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	for (var i = 0, nbmonstersmax; i < nbmonstersmax; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, (i+1)*100, 'Pikachu', 20, 'https://cdn0.iconfinder.com/data/icons/Favorite_monsters/256/pink.png');
		monsters.push(MonsterToCreate);
	}
}

// Fonction qui lance le jeu
function startGame(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel) {
	// On affiche les informations du joueur (html)
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);

	// On lance le décompte

	var timer = setInterval(function() {
		if ($('.restartgame').length >= 1) {
			console.log('caca');
			clearInterval(timer);
		}
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {

			// On arrête le décompte
			clearInterval(timer);

			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
		}
		else {
			Player.time--;
		}
	}, 1000);
}

// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

// Fonction qui calcule l'hypotenuse
function calcHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

// Fonction qui retourne une valeur comprise en % d'un chiffre
function hpPourcent (hp, hpMax) {
	return parseInt(hp * 100 / hpMax);
}
function reset(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel){
	var timer = setInterval(function(){
		if ($('.restartgame').length >= 1) {
			console.log('caca');
			clearInterval(timer);
		}
		if (Player.time <= 0 && monsters.length == 0) {
			Player.time = Player.retime;

			startGame(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel);

			makeMonsters(monsters, Parcours, nbmonstersmax);
		} 
	}, 1000);	
			

}

function restart (Player, Parcours, monsters, towers, nbmonstersmax, maxlevel, remake){
	var timer = setInterval(function(){
		if ($('.restartgame').length >= 1) {
			console.log('caca');
			clearInterval(timer);
		}
		if (Player.level >= maxlevel) {
			$('.monsters').children().remove();
			$('.towers').children().remove();
			$('.container-fluid').children().remove();
			$('.container-fluid').remove();
			var html = '<div class="congratulation" style="display:flex;font-size:72px;font-family:arial;flex-direction:row;text-align:center;align-items:center;justify-content:center;padding:2px;width:100%;height:100vh;"> CONGRATULATION ! </div>';
			$('body').append(html);
			var restart = '<div class="restart" style="display:flex;flex-direction:row;text-align:center;align-items:center;justify-content:center;font-family:Arial,sans-serif;font-size:1.6em;width:100%;height:100vh;padding-top:7px;color:#000;background:#444;background:linear-gradient( #555, #2C2C2C);"><a href="#"> RESTART</a> </div>';
			$('.congratulation').append(restart);	
			boutonrestart(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel, remake);

			clearInterval(timer);
			}
		if (Player.life <= 0) {
			$('.monsters').children().remove();
			$('.towers').children().remove();
			$('.container-fluid').children().remove();
			$('.container-fluid').remove();
			var html1 = '<div class="looser" style="display:flex;font-size:72px;font-family:arial;flex-direction:row;text-align:center;align-items:center;justify-content:center;padding:2px;color:red;width:100%;height:100vh;">DEFEAT</div>';
			$('body').append(html1);
			var restart1 = '<div class="restart1" style="display:flex;flex-direction:row;text-align:center;align-items:center;justify-content:center;font-family:Arial,sans-serif;font-size:1.6em;width:100%;height:100vh;padding-top:7px;color:#000;background:#444;background:linear-gradient( #555, #2C2C2C);"><a href="#"> RESTART</a> </div>';
			$('.looser').append(restart1);	
			boutonrestart(Player, Parcours, monsters, towers, nbmonstersmax, maxlevel, remake);

			clearInterval(timer);
			}
		}, 1000);
}
function boutonrestart (Player, Parcours, monsters, towers, nbmonstersmax, maxlevel, remake){
	var ok = 0;
	$('.restart').on('click',function(e){
		$('.congratulation').remove();
		$('body').append('<div class="restartgame"></div>');
		ok = 1;

	});
	$('.restart1').on('click',function(e){	
		$('.looser').remove();
		$('body').append('<div class="restartgame"></div>');
		ok = 1;
	
	});


	var frame = 0;
	var timer = setInterval(function(){
		if(ok == 1){
			frame++;
			if (frame > 1000 && $('.restartgame').length >= 1) {
				lunchgame(remake, Player.pseudo, Player.difficultie);
				clearInterval(timer);
			}
		}
	}, 1);
}
function menu(remake, nbmonstersmax){
	$('body').keyup(function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if (keycode == '13') {
			var pseudo = $('.pseudo > input').val();
			var difficultie = $('.difficulté > select option').val();
			if (difficultie == 'Débutant') {
				difficultie = 1;
				nbmonstersmax = 5;
			}
			if (difficultie == 'Normal') {
				difficultie = 2;
				nbmonstersmax = 7;
			}
			if (difficultie == 'Expert') {
				difficultie = 3;
				nbmonstersmax = 10
			}
			$('.presentation').remove();
			lunchgame(remake, pseudo, difficultie);
		}
	});
}

